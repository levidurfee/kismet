package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

var db *sql.DB

func init() {
	for {
		db, err := sql.Open("mysql", os.Getenv("DB_USERNAME")+":"+os.Getenv("DB_PASSWORD")+"@tcp("+os.Getenv("DB_HOSTNAME")+":3306)/"+os.Getenv("DB_DATABASE"))
		defer db.Close()
		if err != nil {
			fmt.Println("Could not connect", err)
		}
		if err == nil {
			fmt.Println("Connected to database")
			break
		}
		time.Sleep(5 * time.Second)
	}
}

func main() {
	fmt.Println("Starting Datastore")
	r := mux.NewRouter()
	r.HandleFunc("/user", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("hello"))
	})

	srv := &http.Server{
		Handler:      r,
		Addr:         "0.0.0.0:9090",
		WriteTimeout: 5 * time.Second,
		ReadTimeout:  5 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}
